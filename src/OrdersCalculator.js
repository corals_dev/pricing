import lineItemsJson from './line_items_json.json';
import argv from 'minimist';

export default class OrdersCalculator {
  /**
   *
   */
  constructor() {
    this.lineItems = lineItemsJson;
  }

  /**
   *
   * @param stateCode
   * @param vehicleType
   * @param selectedItem
   * @returns {number}
   */
  calculateOrderItemOptions(stateCode, vehicleType, selectedItem) {
    let sum = 0;

    sum += selectedItem.receive_digital_copy ? this.getLineItemAmountValue('receive_digital_copy', stateCode) : 0;
    sum += selectedItem.receive_physical_copy ? this.getLineItemAmountValue('receive_physical_copy', stateCode) : 0;

    if (selectedItem.renewal_type !== 'duplicate_no_sticker') {
      sum += selectedItem.personal_plate ? this.getLineItemAmountValue('personal_plate_fee', stateCode) : 0;
      sum += selectedItem.is_registration_expired ? this.getLineItemAmountValue('registration_expired_fee', stateCode) : 0;
      sum += selectedItem.is_registration_expired_8_months ? this.getLineItemAmountValue('registration_expired_8_months_fee', stateCode) : 0;
      sum += selectedItem.special_plate ? this.getLineItemAmountValue('special_plate', stateCode) : 0;
    }

    return sum;
  }

  /**
   *
   * @param stateCode
   * @param selectedItems
   * @param formLineItems
   * @param discountObject
   * @returns {*}
   */
  calculateOrderLineItems(stateCode, selectedItems, formLineItems, discountObject = null) {
    this.formLineItems = formLineItems;
    this.selectedItems = selectedItems;
    this.stateCode = stateCode;
    this.discountObject = discountObject;

    this.handleMultipleVehiclesDiscount();
    this.handleProcessingFee();
    this.handleStateRegistrationFee();
    this.handleAgencyFee();
    this.handleDigitalCopy();
    this.handlePhysicalCopy();
    this.handleShippingFee();

    this.handleConvenienceFee();
    this.handleDiscountLineItem()

    return this.formLineItems;
  }

  /**
   *
   */
  handleDiscountLineItem() {
    //1. check if order has discounts
    //2.check the discount type
    //3.push discount line item with its appropriate value
    //3.1 if discount type if line_item get same value of corresponding line_item_value
    //3.2 if percentage or fixed direct flow...

    if (!this.discountObject) {
      //remove promo_code if not found!
      return this.removeLineItemForm('promo_code');
    }

    switch (this.discountObject.type) {
      case 'line_item':
        this.handleLineItemDiscountType();
        break;
      case 'fixed':
        this.handleFixedDiscountType();
        break;
      case 'percentage':
        this.handlePercentageDiscountType();
        break;
    }

  }

  /**
   *
   */
  handlePercentageDiscountType() {
    this.removeLineItemForm('promo_code');

    let orderTotal = this.getOrderTotals(this.stateCode, null, this.formLineItems, true),
      percentageDiscountValue = orderTotal * (this.discountObject.value / 100);

    this.formLineItems.push({
      title: `Promo code [${this.discountObject.code}]`,
      code: 'promo_code',
      amount: this.money(percentageDiscountValue * -1),
      pure_amount: percentageDiscountValue * -1,
      popover_content: 'Promo code'
    });
  }

  handleFixedDiscountType() {

    this.removeLineItemForm('promo_code');


    let orderTotal = this.getOrderTotals(this.stateCode, null, this.formLineItems, true),
      value = this.discountObject.value;

    //if the discount is gt orderTotal take the orderTotal
    if (value > orderTotal) {
      value = orderTotal;
    }

    this.formLineItems.push({
      title: `Promo code [${this.discountObject.code}]`,
      code: 'promo_code',
      amount: this.money(value * -1),
      pure_amount: value * -1,
      popover_content: 'Promo code'
    });
  }

  handleLineItemDiscountType() {
    let lineItemCode = this.discountObject.line_item_code,
      targetLineItem = this.getLineItemForm(lineItemCode);

    if (targetLineItem) {
      this.removeLineItemForm('promo_code');

      this.formLineItems.push({
        title: `Promo code [${this.discountObject.code}]`,
        code: 'promo_code',
        amount: this.money(targetLineItem.pure_amount * -1),
        pure_amount: targetLineItem.pure_amount * -1,
        popover_content: 'Promo code'
      });
    }
  }

  removeLineItemForm(code) {
    return this.formLineItems = this.formLineItems.filter((item) => {
      if (item.code !== code) {
        return item;
      }
    });
  }

  /**
   *
   */
  handleConvenienceFee() {

    let orderTotals = this.getOrderTotals(this.stateCode, null, this.formLineItems, true, ['convenience_fee']),
      convenienceFee = this.getLineItemAmountValue('convenience_fee', this.stateCode, 'default'),
      convenienceLineItem = this.findLineItem('convenience_fee'),
      edgeValue = convenienceLineItem.properties.edge_value,
      serviceFeeLineItem = this.getLineItemForm('service_fee');

    //didn't reach the limit.
    if (!(orderTotals > edgeValue)) {
      return;
    }

    let convenienceValue = orderTotals * (Number(convenienceFee) / 100);

    if (serviceFeeLineItem) {
      serviceFeeLineItem.pure_amount += convenienceValue;
      serviceFeeLineItem.amount = this.money(serviceFeeLineItem.pure_amount);
    } else {

      this.formLineItems.push({
        title: 'Service Fee',
        amount: this.money(convenienceValue),
        code: 'service_fee',
        pure_amount: convenienceValue,
        popover_content: this.getLineItemDescription('service_fee')
      });
    }
  }

  /**
   *
   */
  handleShippingFee() {

    let shippingFee = this.getLineItemForm('shipping_fee'),
      shippingFeeValue = this.getLineItemAmountValue('shipping_fee', this.stateCode, 'default');

    if (shippingFeeValue === 0) {
      this.formLineItems = this.formLineItems.filter((item) => {
        if (item.code !== 'shipping_fee') {
          return item;
        }
      });
      return;
    }

    if (shippingFee) {
      shippingFee.pure_amount = shippingFeeValue;
      shippingFee.amount = this.money(shippingFee.pure_amount);
    } else if (shippingFeeValue) {
      this.formLineItems.push({
        title: "Shipping & Handling",
        pure_amount: shippingFeeValue,
        code: 'shipping_fee',
        amount: this.money(shippingFeeValue),
        popover_content: this.getLineItemDescription('shipping_fee')
      });
    }

    shippingFee = this.getLineItemForm('shipping_fee');

    if (this.allSelectedItemsAre('duplicate_no_sticker')) {

      let v = this.getLineItemAmountValue('shipping_fee', this.stateCode, 'duplicate_no_sticker');

      if (!v) {
        return;
      }

      if (shippingFee) {
        shippingFee.pure_amount = v
        shippingFee.amount = this.money(v);
      } else {
        this.formLineItems.push({
          title: "Shipping & Handling",
          pure_amount: shippingFeeValue,
          code: 'shipping_fee',
          amount: this.money(shippingFeeValue),
          popover_content: this.getLineItemDescription('shipping_fee')
        });
      }

    }
  }

  /**
   *
   * @param renewalType
   * @returns {boolean}
   */
  allSelectedItemsAre(renewalType) {
    return !!this.selectedItems.every(sItem => sItem.renewal_type === renewalType)
  }

  /**
   *
   */
  handleStateRegistrationFee() {

    let totalOrderItems = 0;

    this.selectedItems.forEach(item => {

      let itemBreakDownItems = this.getOrderItemBreakDownItems(item, this.stateCode, item.state_price);
      for (let breakDownItem in itemBreakDownItems) {
        if (['receive_physical_copy', 'receive_digital_copy'].includes(breakDownItem)) {
          continue;
        }

        totalOrderItems += Number(itemBreakDownItems[breakDownItem].price);
      }
    })

    let stateRegistrationFeeLineItem = this.getLineItemForm('est_reg_tax_county_fee');

    if (stateRegistrationFeeLineItem) {
      stateRegistrationFeeLineItem.amount = this.money(totalOrderItems);
      stateRegistrationFeeLineItem.pure_amount = totalOrderItems;
    } else {
      this.formLineItems.push({
        title: "Est. Reg Tax & County Fee",
        amount: this.money(totalOrderItems),
        code: 'est_reg_tax_county_fee',
        pure_amount: totalOrderItems,
        popover_content: this.getLineItemDescription('est_reg_tax_county_fee')
      })
    }


  }

  /**
   *
   */
  handleAgencyFee() {

    let totalAmount = 0,
      agencyFeeLineItem = this.getLineItemForm('agency_fee');

    this.selectedItems.forEach(item => {
      totalAmount += this.getLineItemAmountValue('agency_fee', this.stateCode, item.renewal_type);
    });

    let pureAmount = totalAmount > 0 ? totalAmount : this.getLineItemAmountValue('agency_fee', this.stateCode, 'default');

    if (agencyFeeLineItem) {
      agencyFeeLineItem.pure_amount = pureAmount;
      agencyFeeLineItem.amount = this.money(pureAmount);
    } else {
      this.formLineItems.push({
        title: "Agency Fee",
        amount: this.money(pureAmount),
        code: 'agency_fee',
        pure_amount: pureAmount,
        popover_content: this.getLineItemDescription('agency_fee')
      });
    }

  }

  /**
   *
   */
  handleDigitalCopy() {
    let count = this.selectedItems.filter(sItem => sItem.receive_digital_copy).length;

    if (!count) {
      this.formLineItems = this.formLineItems.filter((item) => {
        if (item.code !== 'receive_a_digital_copy_of_your_renewal') {
          return item;
        }
      });
      return;
    }

    let digitalCopyLineItem = this.getLineItemForm('receive_a_digital_copy_of_your_renewal'),
      totalPureAmount = 0;

    this.selectedItems.forEach(sItem => {
      if (!sItem.receive_digital_copy) {
        return;
      }
      totalPureAmount += this.getLineItemAmountValue('receive_digital_copy', this.stateCode, sItem.renewal_type);
    });

    if (digitalCopyLineItem) {
      digitalCopyLineItem.pure_amount = totalPureAmount;
      digitalCopyLineItem.amount = this.money(digitalCopyLineItem.pure_amount);
      return;
    }

    this.formLineItems.push({
      title: 'Receive a digital copy of your renewal',
      code: 'receive_a_digital_copy_of_your_renewal',
      amount: this.money(totalPureAmount),
      pure_amount: totalPureAmount,
      popover_content: this.getLineItemDescription('receive_digital_copy')
    });
  }

  /**
   *
   */
  handlePhysicalCopy() {
    let count = this.selectedItems.filter(sItem => sItem.receive_physical_copy).length;

    if (!count) {
      this.formLineItems = this.formLineItems.filter((item) => {
        if (item.code !== 'receive_a_physical_copy_of_your_renewal') {
          return item;
        }
      });
      return;
    }

    let physicalCopyLineItem = this.getLineItemForm('receive_a_physical_copy_of_your_renewal'),
      totalPureAmount = 0;

    this.selectedItems.forEach(sItem => {
      if (!sItem.receive_physical_copy) {
        return;
      }
      totalPureAmount += this.getLineItemAmountValue('receive_physical_copy', this.stateCode, sItem.renewal_type);
    });

    if (physicalCopyLineItem) {
      physicalCopyLineItem.pure_amount = totalPureAmount;
      physicalCopyLineItem.amount = this.money(physicalCopyLineItem.pure_amount);
      return;
    }

    this.formLineItems.push({
      title: 'Receive a physical copy of your renewal',
      code: 'receive_a_physical_copy_of_your_renewal',
      amount: this.money(totalPureAmount),
      pure_amount: totalPureAmount,
      popover_content: this.getLineItemDescription('receive_physical_copy')
    });
  }

  /**
   *
   */
  handleMultipleVehiclesDiscount() {

    let discountableSelectedItems = [];

    discountableSelectedItems = this.selectedItems.filter(sItem => {
      return sItem.renewal_type !== 'duplicate_no_sticker';
    });


    let itemsCount = discountableSelectedItems.length - 1,
      multipleLineItemValue = (this.getLineItemAmountValue('service_fee', this.stateCode, 'default') * itemsCount)
        -
        (itemsCount * Number(this.getLineItemAmountValue('multiple_vehicles_fee', this.stateCode)));

    if (multipleLineItemValue > 0) {
      //include the line item

      let oldMultipleVehicleLineItem = this.getLineItemForm('multiple_vehicles_discounts');

      if (oldMultipleVehicleLineItem) {

        oldMultipleVehicleLineItem.amount = this.money(multipleLineItemValue * -1);
        oldMultipleVehicleLineItem.pure_amount = multipleLineItemValue * -1;

      } else {

        this.formLineItems.push({
          title: 'Multiple Vehicles Discounts',
          code: 'multiple_vehicles_discounts',
          amount: this.money(multipleLineItemValue * -1),
          pure_amount: multipleLineItemValue * -1,
          popover_content: this.getLineItemDescription('multiple_vehicles_fee')
        });

      }
    } else {
      //remove the line item
      this.formLineItems = this.formLineItems.filter(item => {
        if (item.code != 'multiple_vehicles_discounts') {
          return item;
        }
      });
    }
  }

  /**
   *
   * @returns {*|string}
   */
  getLineItemDescription(lineItemCode) {
    let lineItem = this.findLineItem(lineItemCode);

    return lineItem ? lineItem.description : '';
  }

  /**
   *
   */
  handleProcessingFee() {


    let processingFeeLineItem = this.getLineItemForm('service_fee'),
      totalAmount = 0;

    this.selectedItems.forEach(item => {
      totalAmount += this.getLineItemAmountValue('service_fee', this.stateCode, item.renewal_type);
    })


    if (processingFeeLineItem) {
      processingFeeLineItem.amount = this.money(totalAmount);
      processingFeeLineItem.pure_amount = totalAmount;
    } else {
      this.formLineItems.push({
        title: 'Service Fee',
        amount: this.money(totalAmount),
        code: 'service_fee',
        pure_amount: totalAmount,
        popover_content: this.getLineItemDescription('service_fee')
      },);
    }
  }

  /**
   *
   * @param code
   * @returns {*}
   */
  getLineItemForm(code) {
    return this.formLineItems.find(item => item.code === code);
  }

  /**
   *
   * @param lineItemCode
   * @returns {any}
   */
  findLineItem(lineItemCode) {
    return this.lineItems.find((item) => item.code === lineItemCode)
  }

  /**
   *
   * @param lineItemCode
   * @param stateCode
   * @param renewalType
   * @returns {number}
   */
  getLineItemAmountValue(lineItemCode, stateCode, renewalType = null) {


    try {

      let lineItem = this.findLineItem(lineItemCode),
        amount = 0;


      if (!lineItem.rates.length) {
        amount = lineItem.amount;
      }


      let rate = lineItem.rates.find(rate => {
        let hasStateCode = rate.state_code.toLowerCase() == stateCode.toLowerCase();
        if (renewalType) {
          return hasStateCode && rate.renewal_type.toLowerCase() == renewalType.toLowerCase();
        }

        return hasStateCode;
      });


      if (rate) {
        amount = rate.amount;
      }

      return Number(amount);
    } catch (e) {
      console.log(e);
      // console.log(lineItemCode);
    }

  }

  /**
   *
   * @param stateCode
   * @param vehicleType
   * @param plate
   */
  getRenewalTypeOptions(stateCode, vehicleType, plate) {
    let orderItemEstimation = this.findLineItem('order_item_estimation'),
      orderItemEstimationRates = orderItemEstimation.rates;

    let renewalTypes = orderItemEstimationRates.filter((rate) => {
      return rate.state_code == stateCode && rate.vehicle_type == vehicleType;
    });

    let renewalTypesOptions = [];

    renewalTypes.forEach(renewalType => {

      let estimate, description, renewal_type, key;

      if (!Number(renewalType.amount)) {
        return;
      }

      switch (renewalType.renewal_type) {
        case 'one_year':
          estimate = `Est. Renewal Fee for 1 year -- ${this.money(renewalType.amount)}`;
          description = '1-Year Renewal';
          key = `${plate.license_plate}_0`;
          renewal_type = 'one_year';
          break;
        case 'two_years':
          estimate = `Est. Renewal Fee for 2 years -- ${this.money(renewalType.amount)}`;
          description = `2-Year Renewal`;
          key = `${plate.license_plate}_1`;
          renewal_type = 'two_years';
          break;
        case 'duplicate_no_sticker':
          estimate = `Duplicate Registration (No Sticker)`;
          description = `Duplicate Registration (No Sticker)`;
          key = `${plate.license_plate}_2`;
          renewal_type = 'duplicate_no_sticker';
          break;
        case 'duplicate_sticker':
          estimate = `Replacement Registration (Yellow Sticker)`;
          description = `Replacement Registration (Yellow Sticker)`;
          key = `${plate.license_plate}_3`;
          renewal_type = 'duplicate_sticker';
          break;
        case 'five_years':
          estimate = `Est. Renewal Fee for 5 years -- ${this.money(renewalType.amount)}`;
          description = `5-Year Renewal`;
          key = `${plate.license_plate}_4`;
          renewal_type = 'five_years';
          break;
      }

      renewalTypesOptions.push({
        'estimate': estimate,
        'licence_plate': plate.license_plate,
        'expires_on': plate.expires_on,
        'price': renewalType.amount,
        'formatted_price': this.money(renewalType.amount),
        'description': description,
        'key': key,
        'renewal_type': renewal_type,
      })
    });

    return renewalTypesOptions;
  }

  /**
   *
   * @param value
   * @returns {string}
   */
  money(value) {
    return new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    }).format(value);
  }

  /**
   *
   * @param selected_item
   * @param stateCode
   * @param statePrice
   * @returns {{}}
   */
  getOrderItemBreakDownItems(selected_item, stateCode, statePrice = 0) {
    let breakDownItems = {};

    statePrice = Number(statePrice);

    if (selected_item.receive_digital_copy) {
      breakDownItems['receive_digital_copy'] = {
        price: this.getLineItemAmountValue('receive_digital_copy', stateCode)
      }
    }

    if (selected_item.receive_physical_copy) {
      breakDownItems['receive_physical_copy'] = {
        price: this.getLineItemAmountValue('receive_physical_copy', stateCode)
      }
    }

    breakDownItems['renewal_price'] = {
      price: statePrice ? statePrice : selected_item.price
    }


    if (statePrice) {
      return breakDownItems;
    }

    if (selected_item.personal_plate) {
      breakDownItems['personal_plate'] = {
        price: this.getLineItemAmountValue('personal_plate_fee', stateCode)
      }
    }

    if (selected_item.is_registration_expired) {
      breakDownItems['is_registration_expired'] = {
        price: this.getLineItemAmountValue('registration_expired_fee', stateCode)
      }
    }


    if (selected_item.is_registration_expired_8_months) {
      breakDownItems['registration_expired_8_months_fee'] = {
        price: this.getLineItemAmountValue('registration_expired_8_months_fee', stateCode)
      }
    }

    if (selected_item.special_plate) {
      breakDownItems['special_plate'] = {
        price: this.getLineItemAmountValue('special_plate', stateCode)
      }
    }

    return breakDownItems;

  }

  /**
   *
   * @param stateCode
   * @param vehicleType
   * @param selectedItem
   * @param priceItem
   * @returns {number}
   */
  getItemPrice(stateCode, vehicleType, selectedItem, priceItem) {
    let orderItemOptionTotal = this.calculateOrderItemOptions(stateCode, vehicleType, selectedItem),
      totalPrice = Number(priceItem.price) + orderItemOptionTotal;


    if (priceItem.renewal_type === 'duplicate_no_sticker' && totalPrice > 8) {
      totalPrice = 8;
    }


    return totalPrice;
  }

  /**
   *
   * @param stateCode
   * @param selectedItems
   * @param formLineItems
   * @param skipCalculateOrderTotals
   * @param exclude
   * @param discountObject
   * @returns {*}
   */
  getOrderTotals(stateCode, selectedItems, formLineItems, skipCalculateOrderTotals = false, exclude = [], discountObject = null) {

    let lineItems;

    if (skipCalculateOrderTotals) {
      lineItems = formLineItems;
    } else {
      lineItems = this.calculateOrderLineItems(stateCode, selectedItems, formLineItems, discountObject)
    }

    return lineItems.reduce((acc, item) => {

      if (exclude.includes(item.code)) {
        return acc;
      }

      let value = Number(item.pure_amount),
        customAmount = Number(item.custom_amount);


      if (item.custom_amount != null) {
        value = Number(customAmount);
      }

      return value + acc;

    }, 0);
  }


  getOrderItemLineItems(stateCode) {
    let orderItemsLineItems = this.lineItems
      .filter(lineItem => {
        return lineItem.line_item_level == 'order_item_level' && lineItem.code != 'order_item_estimation';
      }), lineItemsArray = {};

    orderItemsLineItems.forEach(item => {
      let value = item.amount;

      if (item.rates.length) {
        let rate = item.rates.find(rate => {
          return rate.state_code == stateCode && rate.renewal_type == 'default';
        });

        if (rate) {
          value = rate.amount;
        }
      }

      if (Number(value)) {
        lineItemsArray[item.code] = {
          value: Number(value),
          description: item.description
        };
      }

    });

    return lineItemsArray;

  }

  /**
   *
   */
  findLineItems() {

  }

  //============= special FOR BACKEND ==========\\
  /**
   *
   * @returns {*}
   */
  callcalculateOrderLineItems(argv) {
    let result = this[argv.target_method](argv.state_code, JSON.parse(argv.selected_item), JSON.parse(argv.order_line_items), argv.discount_object);
    return JSON.stringify(result);
  }

  /**
   *
   * @param argv
   * @returns {string}
   */
  callgetOrderTotals(argv) {

    let result = this[argv.target_method](
      argv.state_code,
      JSON.parse(argv.selected_item),
      JSON.parse(argv.order_line_items),
      false,
      [],
      argv.discount_object
    );

    return JSON.stringify(result);
  }

  /**
   *
   * @param argv
   * @returns {*}
   */
  callgetOrderItemBreakDownItems(argv) {
    let result = this[argv.target_method](JSON.parse(argv.selected_item), argv.state_code, argv.state_price);
    return JSON.stringify(result);
  }

  //============= special FOR BACKEND ==========\\
}


let args = argv(process.argv.slice(2));

if (args.target_method) {
  let targetMethodHandler = `call${args.target_method}`;
  let ordersCalculator = new OrdersCalculator();
  console.log(ordersCalculator[targetMethodHandler](args));
}

